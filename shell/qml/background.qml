import QtQuick 2.11
import QtQuick.Window 2.11

Window {
	id: background
	width: Screen.width
	height: Screen.height
	flags: Qt.FramelessWindowHint
	visible: true
	color: '#fff000'
}
